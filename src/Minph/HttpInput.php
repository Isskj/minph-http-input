<?php

namespace Minph;

/**
 * @class Minph\HttpInput
 *
 * User input utility class.
 */
class HttpInput
{

    /**
     * @method get
     * @return array user input data
     */
    public static function get($object = true)
    {
        $data = new \stdClass;
        $raw = file_get_contents('php://input');
        $data->get = empty($_GET) ? new \stdClass : (object)$_GET;
        $data->post = empty($_POST) ? new \stdClass : (object)$_POST;
        $data->raw = empty($raw) ? new \stdClass : $raw;
        $data->header = HttpInput::allHeaders();
        $data->header->BEARER_TOKEN = HttpInput::bearerToken($data->header);
        return $data;
    }

    /**
     * @method bearerToken
     * @return header information of `Authorization: Bearer {TOKEN}`
     */
    private static function bearerToken($header)
    {
        if (isset($header->Authorization)) {
            $auth = $header->Authorization;
            if (preg_match('/^Bearer (.+)$/', $auth, $matches)) {
                if (isset($matches[1])) {
                    return $matches[1];
                }
            }
        }
        return null;
    }
    
    /**
     * @method allHeaders
     * @return all header information
     */
    private static function allHeaders()
    {
        $headers = [];
        if ($_SERVER) {
            foreach ($_SERVER as $name => $value) {
                if (strpos($name, 'HTTP_') === 0) {
                    $headers[substr($name, 5)] = $value;
                } else {
                    $headers[$name] = $value;
                }
            }
        }
        return (object)$headers;
    }
}

