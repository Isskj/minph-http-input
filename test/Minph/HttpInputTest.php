<?php

use PHPUnit\Framework\TestCase;
use Minph\HttpInput;

final class HttpInputTest extends TestCase
{
    public function testHTTPInput()
    {
        $_GET['name'] = 'test-name'; 
        $_POST['id'] = 'test-id';
        $_SERVER['HTTP_CONTENT_TYPE'] = 'application/json';
        $input = HttpInput::get();
        $this->assertEquals($input->get->name, 'test-name');
        $this->assertEquals($input->post->id, 'test-id');
        $this->assertEquals($input->header->CONTENT_TYPE, 'application/json');
    }

}
